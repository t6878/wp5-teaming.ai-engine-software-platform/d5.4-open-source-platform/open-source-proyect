<div align="center">
    <h1>
        OPEN SOURCE PROJECT
        <img src="https://opensource.org/sites/default/files/html/files/osi_keyhole_300X300_90ppi_0.png" alt="OSI Approved License" width="30" height="30"/>
    </h1>
    
</div>

Repository used for researching about open source projects. To set a software development project as an open source project, it is necesary to add a correct license to every public code repository. To ensure that the chosen license is compatible with open source, [OSI-approved open source licenses](https://opensource.org/licenses/alphabetical) will be used through the public repositories of Teaming.AI project. Public repositories must be publicly visible and publicly available.

## Usage
These repository collects all the licenses used in Teaming.AI public repositories and makes it easy to set up a license on a new public repository. License templates are inside [templates](templates) folder. Every license template is inside a folder named as the license preceded by a number, which indicates how restrictive the license is. This allows licenses to be ordered by how restrictive they are, from most restrictive to less restrictive. Here it is an example of the `templates` folder file structure:

``` bash
───┬──templates
···├─────1-GNU Lesser General Public License v3 (LGPL-3.0)
···├──lgpl.txt
···├──lgpl_header.txt
···├─────2-Apache License 2.0 (Apache-2.0)
···├──apache.txt
···├──apache_header.txt
···├─────3-BSD 2-Clause License (FreeBSDSimplified)
···├──bsd.txt
···├─────4-MIT License (Expat)
···└──mit.txt

```

To select a correct license, is neccesary to know the license used by the work done before. The new license must be at least equal restrictive as the most restrictive license used by the code from which we start developing. It is recommended to use the same license.

Once a the license is chosen, the license template has to be copied to the root path of the code repository and renamed as `LICENSE`. Sometimes, a license header has to be included as a comment at the top of the source code files. Here it is an example of a `Apache License 2.0` header inside a python source code:

```python
# Copyright 2021 Teaming.AI

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask
from flask_cors import CORS
from waitress import serve
from helpers.config import ConfigHelper
from routes.machines.machines import machines_route

app = Flask(__name__)
CORS(app)

config = ConfigHelper.instance("config")

app.register_blueprint(machines_route)

if __name__ == "__main__":
    # Run app
    serve(app, **config['SERVE'])

```

## Support
If there is a need for a new license template, do not hesitate to open an issue named as `{{License name}}` where `{{License name}}` should be replaced with the license name which is required. After that, a new branch is created named as `issue\#{Issue number}` where `{{Issue number}}` has to be replaced with the issue number. When the template is finished, a merge request should be created and after approved, the issue will be closed.

For other requests, contact with [Julen Aperribay](https://gitlab.com/JAperri).

## Roadmap
As new public repositories are created for the Teaming.AI, this repository will be populated with new license templates as needed. Nowadays available templates are:

1. [GNU Lesser General Public License v3 (LGPL-3.0)](https://tldrlegal.com/license/gnu-lesser-general-public-license-v3-(lgpl-3))
2. [Apache License 2.0 (Apache-2.0)](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)#summary)
3. [BSD 2-Clause License (FreeBSDSimplified)](https://tldrlegal.com/license/bsd-2-clause-license-(freebsd))
4. [MIT License (Expat)](https://tldrlegal.com/license/mit-license)

## Contributing
Everyone is welcome to populate this repository with new license templates. To add a new license template, open an issue as it is explained on [Support](#support) and follow the procedure. Following the folder naming explained on [Usage](#usage), add a folder with the license template and a license header template if needed.

## Authors and acknowledgment
- [Julen Aperribay](https://gitlab.com/JAperri) from IDEKO.

## License
All licenses in this repository are copyrighted by their respective authors. Everything else is released under [CC0](https://tldrlegal.com/license/creative-commons-cc0-1.0-universal). See [LICENSE](LICENSE) for details.

## Project status
Keep up to date with project status by visiting the following [web page](https://www.teamingai-project.eu/).
